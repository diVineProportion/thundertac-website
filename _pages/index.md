---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
permalink: /
---

# ThunderTac

ThunderTac is a lightweight, standalone program that synchronizes and streams flight data from the War Thunder client in a format compatible with [TacView](https://www.tacview.net/). 
The datastreams from numerous clients can be merged to produce detailed TacView replays, for training, tournament and dogfight analysis.

# FAQ

### How does it work?
ThunderTac is a small, standalone program which runs in the background while you fly in War Thunder. It accesses the real time data that is exposed via http://localhost:8111 while in a match, processes this data and saves it to an .acmi file. This file will contain a 3D, detailed replay of your flight. You can merge that replay with any other participant in the match, if they have also recorded it with ThunderTac. This is very useful in post-match analysis of, for example, 1v1 and 2v2 tournaments, practice engagements and squadron training excercises. 

### Is this Cheating?
No. All flight data comes from the streams provided by War Thunder at http://localhost:8111. You cannot see what opponents are doing in real-time, only after the data streams are merged afterwards, and only clients with ThunderTac installed can supply these streams.

### How can I contribute?
- donations
- git link


